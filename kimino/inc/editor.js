var UI = {
    templates: {
        view: _.template($('#profile-view').html()),
        form: _.template($('#profile-form').html()),
    },
};

$(function  () {
    initStorage();

    UI.listing = $(".profiles-list").sortable({
        nested: false,
        handle: 'span.move-profile',
        onDrop: function ($item, container, _super) {
            saveStorage();

            _super($item, container);
        }
    });

    $("#grpSortThem a.dropdown-item").click(sortProfiles);

    if (isAPIAvailable()) {
        $('nav .import-csv').bind('change', importProfiles);
    } else {
        $('nav .import-csv').hide();
    }

    $("nav a.export-csv").click(exportProfiles);
    $("nav a.new-profile").click(createProfile);

    $("#formProfile .modal-footer .btn-success").click(saveProfile);
});

function removeProfile (ev) {
    bootbox.confirm("Are you sure you want to delete ?", function (answer) {
        if (answer) {
            $(ev.target).parents('li[data-id]').remove();

            saveStorage();
        }
    });
}

function renderProfile (uid) {
    var $target = $('.profiles-list li[data-id="'+uid+'"]');

    $target.find('input[type="checkbox"]').bootstrapToggle({
        size: 'small',
    }).on('change',toggleProfile);

    $target.find('.edit-profile').click(editProfile);
    $target.find('.remove-profile').click(removeProfile);
}

function toggleProfile (ev) {
    var uid = parseInt($(ev.target).parents('li[data-id]').attr('data-id'));

    var data = JSON.parse(localStorage['AmazonMerchProfiles:v0.1']);

    for (var i=0 ; i<data.length ; i++) {
        if (data[i].uid==uid) {
            data[i].enabled = $(ev.target).prop('checked');
        }
    }

    writeStorage(data);
}

function getProfile (uid) {
    var data = JSON.parse(localStorage['AmazonMerchProfiles:v0.1']);

    for (var i=0 ; i<data.length ; i++) {
        if (data[i].uid==uid) {
            return data[i];
        }
    }

    return null;
}

function editProfile (ev) {
    var uid = parseInt($(ev.target).parents('li[data-id]').attr('data-id'));

    var data = getProfile(uid);

    if (data!=null) {
        var dialog = bootbox.dialog({
            title: 'Create a new Profile',
            message: UI.templates.form({
                entry:  data,
            }),
            buttons: {
                confirm: {
                    label: "Save",
                    className: 'btn-success',
                    callback: function() {
                        var item = deduceProfile('#formProfile');

                        resp = UI.templates.view({
                            entry:  item,
                        });

                        $('.profiles-list li[data-id="'+item.uid+'"]').replaceWith(resp);

                        renderProfile(item.uid);

                        saveStorage();
                    }
                },
                cancel: {
                    label: "Cancel",
                    className: 'btn-danger',
                },
            }
        }).on('shown.bs.modal', function (ev) {
            $('#formProfile input[type="checkbox"]').bootstrapToggle({
                size: 'small',
            });
        });
    }
}

function deduceProfile (target) {
    var item = {
        uid:       $(target+' *[name="index-field"]').val(),
        brandName: $(target+' *[name="brand-field"]').val(),
        title:     $(target+' *[name="title-field"]').val(),
        shortcut:  $(target+' *[name="shortcut-field"]').val(),
        price:     $(target+' *[name="price-field"]').val(),
        features:  [
            $(target+' *[name="feature-one-field"]').val(),
            $(target+' *[name="feature-two-field"]').val(),
        ],
        description: $(target+' *[name="description-field"]').val(),
        enabled: $(target+' *[name="enabled-field"]').prop('checked'),
    };

    item.uid     = parseInt(item.uid);
    item.price   = parseFloat(item.price.replace(',','.'));

    return item;
}

function saveProfile (ev) {
    var item = deduceProfile('#formProfile');

    resp = UI.templates.view({
        entry:  item,
    });

    $('.profiles-list li[data-id="'+item.uid+'"]').replaceWith(resp);

    $("#formProfile").modal('hide');

    renderProfile(item.uid);

    saveStorage();
}

function createProfile (ev) {
    var data = {
        uid:       $(".profiles-list li").length,
        brandName: "",
        title:     "",
        shortcut:  "",
        price:     "",
        features:  [
            "",
            "",
        ],
        description: "",
        enabled:   false,
    };

    var dialog = bootbox.dialog({
        title: 'Create a new Profile',
        message: UI.templates.form({
            entry:  data,
        }),
        buttons: {
            confirm: {
                label: "Save",
                className: 'btn-success',
                callback: function() {
                    var item = deduceProfile('#formProfile');

                    resp = UI.templates.view({
                        entry:  item,
                    });

                    $('.profiles-list').append(resp);

                    renderProfile(item.uid);

                    saveStorage();
                }
            },
            cancel: {
                label: "Cancel",
                className: 'btn-danger',
            },
        }
    }).on('shown.bs.modal', function (ev) {
        $('#formProfile input[type="checkbox"]').bootstrapToggle({
            size: 'small',
        });
    });
}

function initStorage () {
    var data = localStorage['AmazonMerchProfiles:v0.1'];

    if (data=='undefined' || data==undefined) {
        localStorage['AmazonMerchProfiles:v0.1'] = '[]';
    }

    loadStorage();
}

function loadStorage () {
    var data = JSON.parse(localStorage['AmazonMerchProfiles:v0.1']);

    $(".profiles-list").empty();

    for (var i=0 ; i<data.length ; i++) {
        $(".profiles-list").append(UI.templates.view({
            entry:  data[i],
        }));
    }

    for (var i=0 ; i<data.length ; i++) {
        renderProfile(data[i].uid);
    }

    var algo = localStorage["AmazonMerchOrdering:v0.1"];

    $('#grpSortThem .dropdown-item').removeClass('active');

    $('#grpSortThem .dropdown-item[data-field="'+algo+'"]').addClass('active');
}

function readProfiles () {
    var lst = UI.listing.sortable("serialize").get()[0];

    var resp=[], item, $elem;

    for (var i=0 ; i<lst.length ; i++) {
        $elem = $('.profiles-list li[data-id="'+lst[i].id+'"]');

        $feat = $elem.find(".field-feature");

        item = {
            uid:       lst[i].id,
            brandName: $elem.find(".field-brand").text(),
            title:     $elem.find(".field-title").text(),

            price:     $elem.find(".field-price").text(),
            features:  [],
            description: $elem.find(".field-description").text(),

            shortcut:  $elem.find(".field-shortcut").text(),
            enabled:   $elem.find(".field-enabled").prop('checked'),
        };

        for (var j=0 ; j<$feat.length ; j++) {
            value = $feat[j].innerText || "";

            if (value.trim().length!=0) {
                item.features.push(value);
            }
        }

        item.price   = parseFloat(item.price.replace('$','').replace(',','.'));

        resp.push(item);
    }

    return resp;
}

function saveStorage () {
    var resp = readProfiles();

    writeStorage(resp);
}

function writeStorage (resp) {
    var algo = localStorage["AmazonMerchOrdering:v0.1"];

    if (algorithms[algo]!=null) {
        resp = resp.sort(algorithms[algo]);
    }

    localStorage["AmazonMerchProfiles:v0.1"] = JSON.stringify(resp);

    if (typeof chrome !== 'undefined') {
        chrome.runtime.sendMessage({
            action: window.ampActions.refreshExtMenus,
            data: resp,
        });
    }

    loadStorage();
}

var algorithms = {
    title: function (a,b) {
        if (a.title < b.title)
            return -1;
        if (a.title > b.title)
            return 1;
        return 0;
    },
    brand: function (a,b) {
        if (a.brandName < b.brandName)
            return -1;
        if (a.brandName > b.brandName)
            return 1;
        return 0;
    },
    price: function (a,b) {
        if (a.price < b.price)
            return -1;
        if (a.price > b.price)
            return 1;
        return 0;
    },
};

function sortProfiles (ev) {
    var key = $(ev.target).attr('data-field');

    if (algorithms[key]!=null) {
        var resp = readProfiles();

        resp = resp.sort(algorithms[key]);

        writeStorage(resp);
    }

    localStorage["AmazonMerchOrdering:v0.1"] = key;

    loadStorage();
}

function exportProfiles (ev) {
    var resp = readProfiles();

    downloadFileFromText('amazon-merch-profiles.csv', $.csv.fromObjects(resp));
}

function downloadFileFromText(filename, content) {
    var a = document.createElement('a');
    var blob = new Blob([ content ], {type : "text/plain;charset=UTF-8"});
    a.href = window.URL.createObjectURL(blob);
    a.download = filename;
    a.style.display = 'none';
    document.body.appendChild(a);
    a.click(); //this is probably the key - simulating a click on a download link
    delete a;// we don't need this anymore
}

function importProfiles(evt) {
    /*
    var data = null;
    
    writeStorage(resp);
    //*/

    var files = evt.target.files; // FileList object
    var file = files[0];

    // read the file metadata
    var output = ''
        output += '<span style="font-weight:bold;">' + escape(file.name) + '</span><br />\n';
        output += ' - FileType: ' + (file.type || 'n/a') + '<br />\n';
        output += ' - FileSize: ' + file.size + ' bytes<br />\n';
        output += ' - LastModified: ' + (file.lastModifiedDate ? file.lastModifiedDate.toLocaleDateString() : 'n/a') + '<br />\n';

    var reader = new FileReader();
    reader.readAsText(file);
    reader.onload = function(event){
        var csv = event.target.result;
        var data = $.csv.toArrays(csv);
        var html = '';
        var meta = null;

        var resp = readProfiles();

        for(var row in data) {
            if (row==0) {
                meta = data[row];
            } else {
                var entry = {};

                for(var item in data[row]) {
                    entry[meta[item]] = data[row][item];
                }

                entry['uid']      = parseInt(entry['uid']);
                entry['price']    = parseFloat(entry['price']);
                entry['enabled']  = (entry['enabled']=='true')?true:false;
                entry['features'] = entry['features'].split(',');

                resp.push(entry);

                $(".profiles-list").append(UI.templates.view({
                    entry: entry,
                }));

                renderProfile(entry.uid);
            }
        }

        writeStorage(resp);
    };
    reader.onerror = function(){ alert('Unable to read ' + file.fileName); };
}

function isAPIAvailable() {
    // Check for the various File API support.
    if (window.File && window.FileReader && window.FileList && window.Blob) {
        // Great success! All the File APIs are supported.
        return true;
    } else {
        // source: File API availability - http://caniuse.com/#feat=fileapi
        // source: <output> availability - http://html5doctor.com/the-output-element/
        document.writeln('The HTML5 APIs used in this form are only available in the following browsers:<br />');
        // 6.0 File API & 13.0 <output>
        document.writeln(' - Google Chrome: 13.0 or later<br />');
        // 3.6 File API & 6.0 <output>
        document.writeln(' - Mozilla Firefox: 6.0 or later<br />');
        // 10.0 File API & 10.0 <output>
        document.writeln(' - Internet Explorer: Not supported (partial support expected in 10.0)<br />');
        // ? File API & 5.1 <output>
        document.writeln(' - Safari: Not supported<br />');
        // ? File API & 9.2 <output>
        document.writeln(' - Opera: Not supported');
        return false;
    }
}

