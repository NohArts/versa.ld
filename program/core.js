var UI = {
    bpath: '/kimino',
    rpath: function (x) { return UI.bpath+'/'+x; },
    themes: [
        /*
        'bin-jradi','lachhab','octavio-aburtod','producty','sun-down',
        'sparklight','that-circle','the-gaze','the-ride',
        'Austinmer-sunrise','Cathedral-Rocks','Curracurrong',
        'Nugget-Point','Seacliff-bridge','Sheer-Wood','Yosemite-Park',
        'Wollongong-Lighthouse','Wollongong-Storm','world-in-a-new-light',
        //*/

        'violate','lights','city','greenish','night','blue','sunny',
        'ocean','sunset','yellow','kiwi','nexus',
        // 'cloth','tectile'
    ],
    widgets: [{
        type: 'profile', text: "Howdy, Buddy", data: { }
    },{
        type: 'agenda', text: "Timeline", data: { }
    },{
        type: 'listing', text: "Ongoing Work", data: { }
    },{
        type: 'cables', text: "World-wide", data: { }
    }],
    menus: [{
        name: 'person', icon: "home", text: "Identity",
        skin: "city", link: "#", list: [{
            link: 'index.html',  name: "Overview",
        },{
            link: 'vcard.html',  name: "Contacts",
        },{
            link: 'inbox.html',  name: "Mailbox",
        },{
            link: 'agenda.html', name: "Agendas",
        },{
            link: 'history.html', name: "History & Bookmarks",
        },{
            link: 'theques.html', name: "Mediatheque",
        }],
    },{
        name: 'console', icon: "widget", text: "Console",
        skin: "sunny", link: "#", list: [{
            link: 'index.html', name: "Homepage",
        },{
            link: 'sql.html', name: "SQL",
        },{
            link: 'sparql.html', name: "RDF / SPARQL",
        },{
            link: 'cypher.html', name: "Neo4j / Cypher",
        },{
            link: 'graphql.html', name: "GraphQL",
        },{
            link: 'solid.html', name: "Solid Server",
        },{
            link: 'parse.html', name: "Parse Server",
        },{
            link: 'backbone.html', name: "BackBone.js",
        }],
    },{
        name: 'linked', icon: "page", text: "Linked Data",
        skin: "lights", link: "#", list: [{
            link: 'index.html',   name: "Overview",
        },{
            link: 'corpora.html', name: "Corporas",
        },{
            link: 'dialect.html', name: "Dialects",
        },{
            link: 'grammar.html', name: "Grammars",
        },{
            link: 'domains.html', name: "Domains",
        }],
    },{
        name: 'records', icon: "chart", text: "Big Data",
        skin: "night", link: "#", list: [{
            link: 'index.html', name: "Overview",
        },{
            link: 'atomics.html', name: "Data Listing",
        },{
            link: 'metrics.html', name: "Metrics &amp; Analytis",
        },{
            link: 'catalog.html', name: "Semantic Catalog",
        },{
            link: 'tabular.html', name: "Tabular Data",
        }],
    },{
        name: 'explore', icon: "folder", text: "Virtual File System",
        skin: "sunset", link: "#", list: [{
            link: 'index.html', name: "Root Directory",
        },{
            link: 'media.html', name: "Media",
        },{
            link: 'plugin.html', name: "Components",
        },{
            link: 'gallery.html', name: "Gallery",
        },{
            link: 'typos.html', name: "Typography",
        }],
    },{
        name: 'example', icon: "photos", text: "Theme Examples",
        skin: "ocean", link: "#", list: [{
            link: 'index.html', name: "Homepage",
        },{
            link: 'alerts.html', name: "Alerts",
        },{
            link: 'buttons.html', name: "Buttons",
        },{
            link: 'images.html', name: "Images",
        },{
            link: 'labels.html', name: "Labels",
        },{
            link: 'widgets.html', name: "Widgets",
        }],
    }],
    events: [{
        when: "2 Hours ago", icon: "/kimino/img/profile-pics/1.jpg",
        name: "Nadin Jackson", link: "#",
        text: "Mauris consectetur urna nec tempor adipiscing. Proin sit amet nisi ligula. Sed eu adipiscing lectus",
    }],
    loader: [],
};

var Socklet = {
    hook: {
        open: function (session) {
            // 1) subscribe to a topic
            function onevent(args) {
               console.log("Event:", args[0]);
            }
            session.subscribe('com.myapp.hello', onevent);

            // 2) publish an event
            session.publish('com.myapp.hello', ['Hello, world!']);

            // 3) register a procedure for remoting
            function add2(args) {
               return args[0] + args[1];
            }
            session.register('com.myapp.add2', add2);

            // 4) call a remote procedure
            session.call('com.myapp.add2', [2, 3]).then(
               function (res) {
                  console.log("Result:", res);
               }
            );
        },
        close: function (session) {

        },
    },
    connect: function (alias, config, handle) {
        if (handle.auth!=null) {
            config.onchallenge = handle.auth;
        }

        var resp = new autobahn.Connection(config);

        resp.onopen  = handle.open  || Socklet.hook.open;
        resp.onclose = handle.close || Socklet.hook.close;

        Socklet.wamp[alias] = resp ; Socklet.list.push(alias);

        resp.open();

        return resp;
    },
    wamp: {},
    list: [],
};
