Socklet.connect('default', {
    url:   'ws://localhost:8000/spine',
    realm: 'uchikoma',

    authmethods: ["ticket"],
    authid: "joe",
}, {
    auth: function (session, method, extra) {
        console.log("onchallenge", method, extra);

        if (method === "ticket") {
            return "secret";
        } else {
            throw "don't know how to authenticate using '" + method + "'";
        }
    },
    open: function (session, details) {
        console.log("connected session with ID " + session.id);
        console.log("authenticated using method '" + details.authmethod + "' and provider '" + details.authprovider + "'");
        console.log("authenticated with authid '" + details.authid + "' and authrole '" + details.authrole + "'");

        $('#wamp_default').css('background-color','green');

        // 1) subscribe to a topic
        function onevent(args) {
           console.log("Event:", args[0]);
        }
        session.subscribe('com.myapp.hello', onevent);

        // 2) publish an event
        session.publish('com.myapp.hello', ['Hello, world!']);

        // 3) register a procedure for remoting
        function add2(args) {
           return args[0] + args[1];
        }
        session.register('com.myapp.add2', add2);

        // 4) call a remote procedure
        session.call('com.myapp.add2', [2, 3]).then(
           function (res) {
              console.log("Result:", res);
           }
        );
    },
    close: function (session) {
        $('#wamp_default').css('background-color','red');
    },
});
