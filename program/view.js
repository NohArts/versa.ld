$(document).ready(function(){
    var theme='violate', curr=window.location.pathname, bread=[
        { text: "Home", link: '/landing.html' }
    ], axe=null, resp;

    resp = "";
        for (var i=0; i<UI.menus.length ; i++) {
            item = UI.menus[i];

            item.type = '';

            if (item.link!='#') {
                if (item.link==curr) {
                    axe = item.name;
                    item.type = 'active';

                    bread.push({
                        text: item.text,
                        link: item.link,
                    });
                }
            } else {
                for (var j=0; j<item.list.length ; j++) {
                    item.list[j].path = '/'+item.name+'/'+item.list[j].link;

                    if (item.list[j].path==curr) {
                        axe = item.name;
                        item.type += ' active';

                        theme = item.skin;

                        bread.push({
                            text: item.text,
                            link: '/'+item.name+'/index.html',
                        });

                        bread.push({
                            text: item.list[j].name,
                            link: item.list[j].path,
                        });
                    }
                }

                item.type += ' dropdown';
            }

            resp += '<li class="'+item.type+'">';

            resp += '<a class="sa-side-'+item.icon+'" href="'+item.path+'">';
            resp += '<span class="menu-item">'+item.text+'</span></a>';

            if (item.link=='#') {
                resp += '<ul class="list-unstyled menu-item">';

                for (var j=0; j<item.list.length ; j++) {
                    resp += '<li><a href="'+item.list[j].path+'">'+item.list[j].name+'</a></li>';
                }

                resp += '</ul>';
            }

            resp += '</li>';
        }
    $('#sidebar div.side-widgets').empty();
        var cnt = {
            widget: null,
        };

        for (var i=0; i<UI.widgets.length ; i++) {
            wdg = UI.widgets[i];

            wdg.path = '/kimino/nav/widget/'+wdg.type+'.html';

            $.get(wdg.path, function(data, textStatus, XMLHttpRequest) {
                tpl = swig.compile(data);

                cnt.widget = wdg;

                $('#sidebar div.side-widgets').append(tpl(cnt));
            });
        }
    $('#sidebar ul.side-menu').empty().append(resp);

    resp = "";
        for (var i=0; i<bread.length ; i++) {
            resp += '<li class="'+((i==bread.length-1)?'active':'')+'">';
            resp += '<a href="'+bread[i].link+'">'+bread[i].text+'</a>';
            resp += '</li>';
        }
    $('#content ol.breadcrumb').empty().append(resp);

    resp = "";
        for (var i=0; i<UI.events.length ; i++) {
            item = UI.events[i];

            resp += '<div class="media">';

            resp += '<div class="pull-left"><img width="40" src="'+item.icon+'" alt=""></div>';
            resp += '<div class="media-body"><small class="text-muted">'+item.name+' - '+item.when+'</small><br>';
            resp += '<a class="t-overflow" href="'+item.link+'">'+item.text+'</a>';

            resp += '</div></div>';
        }
    $('#notifications div.overflow').empty().append(resp);

    /* --------------------------------------------------------
	Template Settings
    -----------------------------------------------------------*/

    $('body').attr('id', 'skin-blur-'+theme);

    resp = '';
        for (var i=0 ; i<UI.themes.length ; i++) {
            skin = {
                alias: UI.themes[i],
                //cover: UI.rpath('skin/'+UI.themes[i]+'.jpg'),
                cover: UI.rpath('img/body/'+UI.themes[i]+'.jpg'),
            };

            resp += '<a data-skin="skin-blur-'+skin.alias+'" class="col-sm-2 col-xs-4" href="">';
	        resp += '<img src="'+skin.cover+'" alt="'+skin.alias+'"></a>';
        }
    $('#main').prepend('<a id="settings" href="#changeSkin" data-toggle="modal">' +
		'<i class="fa fa-gear"></i> Change Skin' +
    '</a>' +
    '<div class="modal fade" id="changeSkin" tabindex="-1" role="dialog" aria-hidden="true">' +
		'<div class="modal-dialog modal-lg">' +
		    '<div class="modal-content">' +
			    '<div class="modal-header">' +
			        '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' +
			        '<h4 class="modal-title">Change Template Skin</h4>' +
			    '</div>' +
			    '<div class="modal-body">' +
	                '<div class="row template-skins">' +
            resp +
                    '</div>' +
			    '</div>' +
		    '</div>' +
		'</div>' +
    '</div>');

    $('body').on('click', '.template-skins > a', function(e){
	    e.preventDefault();
	    var skin = $(this).attr('data-skin');
	    $('body').attr('id', skin);
	    $('#changeSkin').modal('hide');
    });
});
